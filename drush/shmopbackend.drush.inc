<?php

/**
 * @file
 * Drush integration for the shmopbackend module.
 */
 
use Drupal\shmopbackend\ShmopBackend;

/**
 * Implements hook_drush_command().
 */
function shmopbackend_drush_command() {
  $items = array();

  // The 'get' command.
  $items['shmopbackend-get'] = array(
    'description' => "Fetch a cached object and display it.",
    'arguments' => array(
      'bin' => 'The cache bin to fetch from.',
      'cid' => 'The id of the object to fetch.',
    ),
    'examples' => array(
      'drush shmopbackend-get config system.site' => "Dump the 'system.site' value of the 'config' cache bin.",
    ),
    'outputformat' => array(
      'default' => 'print-r',
      'pipe-format' => 'var_export',
      'output-data-type' => TRUE,
    ),
    'aliases' => array('shmget'),
    // No bootstrap at all.
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  
  // The 'delete' command
  $items['shmopbackend-delete'] = array(
    'description' => "Delete an object from a cache bin.",
    'arguments' => array(
      'bin' => 'The cache bin to delete from.',
      'cid' => 'The id of the object to delete.',
    ),
    'examples' => array(
      'drush shmopbackend-delete config system.site' => "Delete the 'system.site' item from the 'config' cache bin.",
    ),
    'aliases' => array('shmdel'),
    // No bootstrap at all.
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  
  // The 'index-get' command
  $items['shmopbackend-dump'] = array(
    'description' => "Display the contents of a cache bin.",
    'arguments' => array(
      'bin' => 'The cache bin.',
    ),
    'examples' => array(
      'drush shmopbackend-dump config' => "Display the contents of the 'config' cache bin.",
    ),
    'outputformat' => array(
      'default' => 'print-r',
      'pipe-format' => 'var_export',
      'output-data-type' => TRUE,
    ),
    'aliases' => array('shmdump'),
    // No bootstrap at all.
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  return $items;
}

/**
 * Print an object returned from the cache.
 *
 * @param $bin
 *   A specific bin to fetch from.
 * @param $cid
 *   The cache ID of the object to fetch.
 */
function drush_shmopbackend_get($bin, $cid) {
  $shmop = new ShmopBackend($bin);
  if($result = $shmop->get($cid)) {
    return($result);
  } else {
    return drush_set_error('DRUSH_CACHE_OBJECT_NOT_FOUND', dt('The !cid object in the !bin cache bin was not found.', array('!cid' => $cid, '!bin' => $bin)));
  }
}

/**
 * Delete an object from a cache bin.
 *
 * @param $bin
 *   A specific bin to delete from.
 * @param $cid
 *   The cache ID of the object to delete.
 */
function drush_shmopbackend_delete($bin, $cid) {
  $shmop = new ShmopBackend($bin);
  if($shmid = $shmop->delete($cid)) {
    drush_log(dt("'!cid' cache item at shmid !shmid was deleted from bin '!bin'.", array('!cid' => $cid, '!shmid' => $shmid, '!bin' => $bin)), 'success');
  } else {
    return drush_set_error('DRUSH_CACHE_OBJECT_NOT_FOUND', dt('The !cid object in the !bin cache bin was not found.', array('!cid' => $cid, '!bin' => $bin)));
  }
}

/**
 * Dump a cache bin.
 *
 * @param $bin
 *   A specific bin.
 */
function drush_shmopbackend_dump($bin) {
  $shmop = new ShmopBackend($bin);
  $cache = $shmop->getCache();
  if(is_array($cache)) {
    return($cache);
  } else {
    return drush_set_error('DRUSH_CACHE_BIN_NOT_FOUND', dt('The !bin cache bin was not found.', array('!bin' => $bin)));
  }
}
