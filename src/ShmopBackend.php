<?php

/**
 * @file
 * Definition of Drupal\shmopbackend\ShmopBackend.
 */

namespace Drupal\shmopbackend;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Site\Settings;
use Drupal\shmopbackend\ShmopBackendIndex;

/**
 * Defines a shmop cache implementation.
 *
 */
class ShmopBackend implements CacheBackendInterface {

  /**
   * The cache bin to use.
   *
   * @var string
   */
  protected $bin;
  
  /**
   * The cache bin key, required for deletion and invalidation.
   *
   * @var string
   */
  protected $key;

  /**
   * Constructs a ShmopBackend object.
   *
   * @param string $bin
   *   The cache bin for which the object is created.
   */
  public function __construct($bin) {
    $this->bin = $bin;
    $this->key = (string) crc32(Settings::getHashSalt()) . ':' . $this->bin;
  }

  /**
   * Implements Drupal\Core\Cache\CacheBackendInterface::get().
   */
  public function get($cid, $allow_invalid = FALSE) {
    @$shmid = shmop_open($this->normalizeCid($cid), 'a', 0, 0);
    if(!empty($shmid)) {
      $sdata = shmop_read($shmid, 0, shmop_size($shmid));
      if(ord($sdata) > 0) {
        $cache = unserialize($sdata);
        shmop_close($shmid);
      } else {
        shmop_close($shmid);
        \Drupal::logger('shmopbackend')->warning('Could not read cache item %cid.', array('%cid' => $cid));
        return FALSE;
      }
    } else {
      return FALSE;
    }
    
    if($cache) {
      $cache->valid = $cache->expire == Cache::PERMANENT || $cache->expire >= REQUEST_TIME;
    } else {
      return FALSE;
    }
    
    if (!$allow_invalid && !$cache->valid) {
      return FALSE;
    }
    return $cache;
  }

  /**
   * Implements Drupal\Core\Cache\CacheBackendInterface::getMultiple().
   */
  public function getMultiple(&$cids, $allow_invalid = FALSE) {
    $ret = array();
    foreach ($cids as $cid) {
      if ($item = $this->get($cid, $allow_invalid)) {
        $ret[$item->cid] = $item;
      }
    }
    $cids = array_diff($cids, array_keys($ret));
    return $ret;
  }

  /**
   * Implements Drupal\Core\Cache\CacheBackendInterface::set().
   */
  public function set($cid, $data, $expire = Cache::PERMANENT, array $tags = array()) {
    $tags[] = 'shmop:' . $this->key;
    Cache::validateTags($tags);
    $item = (object) array(
      'cid' => $cid,
      'data' => $data,
      'created' => round(microtime(TRUE), 3),
      'expire' => $expire,
      'tags' => array_unique($tags),
    );
    $this->writeItem($cid, $item);
  }

  /**
   * {@inheritdoc}
   */
  public function setMultiple(array $items = array()) {
    foreach ($items as $cid => $item) {
      $this->set($cid, $item['data'], isset($item['expire']) ? $item['expire'] : CacheBackendInterface::CACHE_PERMANENT, isset($item['tags']) ? $item['tags'] : array());
    }
  }

  /**
   * Implements Drupal\Core\Cache\CacheBackendInterface::delete().
   */
  public function delete($cid) {
    @$shmid = shmop_open($this->normalizeCid($cid), 'a', 0, 0);
    if (!empty($shmid)) {
      shmop_delete($shmid);
      shmop_close($shmid);
      //Return the item shmid in case of success
      return($shmid);
    } else {
      //Return FALSE in case the item was not found
      return FALSE;
    }
  }

  /**
   * Implements Drupal\Core\Cache\CacheBackendInterface::deleteMultiple().
   */
  public function deleteMultiple(array $cids) {
    foreach ($cids as $cid) {
      $this->delete($cid);
    }
  }

  /**
   * Implements Drupal\Core\Cache\CacheBackendInterface::deleteAll().
   */
  public function deleteAll() {
    $cache = $this->getCache();
    foreach ($cache as $key=>$item) {
      @$shmid = shmop_open($key, 'a', 0, 0);
      if(!empty($shmid)) {
        shmop_delete($shmid);
        shmop_close($shmid);
      }
    }
  }

  /**
   * Implements Drupal\Core\Cache\CacheBackendInterface::deleteTags().
   */
  public function deleteTags(array $tags) {
    $cache = $this->getCache();
    foreach ($cache as $key=>$item) {
      if (array_intersect($tags, $item->tags)) {
        $this->delete($item->cid);
      }
    }
  }

  /**
   * Implements Drupal\Core\Cache\CacheBackendInterface::invalidate().
   */
  public function invalidate($cid) {
    if($cache = $this->get($cid)) {
      $this->set($cid, $cache->data, REQUEST_TIME - 1, $cache->tags);
    }
  }

  /**
   * Implements Drupal\Core\Cache\CacheBackendInterface::invalidateMultiple().
   */
  public function invalidateMultiple(array $cids) {
    foreach ($cids as $cid) {
      $this->invalidate($cid);
    }
  }

  /**
   * Implements Drupal\Core\Cache\CacheBackendInterface::invalidateTags().
   */
  public function invalidateTags(array $tags) {
    $cache = $this->getCache();
    foreach ($cache as $key=>$item) {
      if (array_intersect($tags, $item->tags)) {
        $item->expire = REQUEST_TIME - 1;
        $this->writeItem($item->cid, $item);
      }
    }
  }

  /**
   * Implements Drupal\Core\Cache\CacheBackendInterface::invalidateAll().
   */
  public function invalidateAll() {
    $cache = $this->getCache();
    foreach ($cache as $key => $item) {
      $item->expire = REQUEST_TIME - 1;
      $this->writeItem($item->cid, $item);
    }
  }

  /**
   * Implements Drupal\Core\Cache\CacheBackendInterface::garbageCollection().
   */
  public function garbageCollection() {}

  /**
   * {@inheritdoc}
   */
  public function removeBin() {
    $this->deleteAll();
  }
  
  /**
   * Writes a normalized cache item in the cache bin.
   */
  protected function writeItem($cid, $item) {
    $sdata = serialize($item);
    $ldata = strlen($sdata);
    
    @$shmid = shmop_open($this->normalizeCid($cid), 'a', 0, 0);
    if(!empty($shmid)) {
      //To resize the block, delete it
      shmop_delete($shmid);
      shmop_close($shmid);
    }
    
    @$shmid = shmop_open($this->normalizeCid($cid), 'c', 0666, $ldata);
    if(!empty($shmid)) {
      shmop_write($shmid, $sdata, 0);
      shmop_close($shmid);
      return($shmid);
    } else {
      \Drupal::logger('shmopbackend')->warning('Could not set cache item %cid.', array('%cid' => $cid));
      return FALSE;
    }
  }
  
  /**
   * Returns the cache bin as an array of values.
   */
  public function getCache() {
    $ret = array();
    $rows = array();
    
    //List existing shared memory segments
    exec('ipcs -m', $rows);
    
    //For each line returned by the previous command, check if the shmop_key tag exists and equals the current cache bin key.
    //If yes, the item belongs to the current cache bin, add it to the returned array.
    foreach ($rows as $row) {
      $values = preg_split("/[\s]+/", $row);
      if(isset($values[1])) {
        $key = $values[0];
        @$shmid = shmop_open($key, 'a', 0, 0);
        if(!empty($shmid)) {
          $sdata = shmop_read($shmid, 0, shmop_size($shmid));
          shmop_close($shmid);
          if(ord($sdata) > 0) {
            if($cache = unserialize($sdata)) {
              if(isset($cache->tags) && in_array('shmop:' . $this->key, $cache->tags)) {
                $ret[$key] = $cache;
              }
            }
          }
        }
      }
    }
    return $ret;
  }
  
  /**
   * Ensures a normalized cache ID.
   *
   * @param string $cid
   *   The passed in cache ID.
   *
   * @return string
   *   A normalized cache ID.
   */
  protected function normalizeCid($cid) {
    return crc32($this->key . ':' . $cid);
  }
}
