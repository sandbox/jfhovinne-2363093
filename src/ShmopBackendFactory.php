<?php

/**
 * @file
 * Contains \Drupal\shmopbackend\ShmopBackendFactory.
 */

namespace Drupal\shmopbackend;

use Drupal\Core\Cache\CacheFactoryInterface;

class ShmopBackendFactory implements CacheFactoryInterface {

  /**
   * {@inheritdoc}
   */
  function get($bin) {
    return new ShmopBackend($bin);
  }

}
